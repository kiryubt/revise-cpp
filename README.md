# README
This repository is not for publication and is purely for educational use only.

## Prequisite
The following are tools that you may install for this repository.

*  CMake  
*  Git  
*  Sourcetree (optional)  
*  Text Editor of your choice  
*  GNU Compiler  
*  Clang Compiler  

## Building and Running
Ideally there will be a CMakeLists.txt involved in however many sub-projects that we have in this repo.  
  
However, if a CMakeLists.txt is not present and you're unable to compile it with CMake, manually run the code with the following flags:  
  
**GNU**
```bash
g++ -std=c++1z -Wall -Wextra -Wconversion <file_name(s)> -o <output_name>
```

**CLANG**
```bash
clang++ -std=c++1z -stdlib=c++ -Wall -Wextra -Wconversion <file_name(s)> -o <output_name>
```