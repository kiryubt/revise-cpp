#include "library.h"

namespace Library {

  std::string Book::get_author() const noexcept
  {
    return mAuthor;
  }

  std::string Book::get_title() const noexcept
  {
    return mTitle;
  }
}