#pragma once

#include <string>

namespace Library {

  /**
   * @brief A book that is either:
   *        1) Borrowed from the library
   *        2) Stored in the library
   */
  class Book
  {
  public:
    Book(const std::string& author, const std::string& title)
      : mAuthor{author}, mTitle{title}
    {
    }

    /**
     * @brief Get the author's name of the current book
     */
    std::string get_author() const noexcept;

    /**
     * @brief Get the title's name of the current book
     */
    std::string get_title() const noexcept;

  private:
    std::string mAuthor;
    std::string mTitle;
  };
}